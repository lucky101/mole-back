import logging

from rest_framework import permissions

from .serializers import GrandTokenSerializer


logger = logging.getLogger(__name__)


class RegistredDeviceRequired(permissions.BasePermission):

    def has_permission(self, request, view):
        return request.device


class RegisterPermission(permissions.BasePermission):

    def has_permission(self, request, view):
        token = view.kwargs.get("token")
        token_data = {
            "grand_token": token
        }
        token_serializer = GrandTokenSerializer(data=token_data)
        if token_serializer.is_valid():
            return True
        logger.info(f"Permission denied for request with data {token_data}")
        return False
