import logging
from smtplib import SMTPException

from django.core.mail import send_mail
from django.template.loader import render_to_string


logger = logging.getLogger(__name__)


class MailSender:

    def __init__(self, template=None, subject="", from_email=None, recipients=[], html_message=None, message=None, request=None, context={}, silent=False):
        self.template = template
        self.subject = subject
        self.from_email = from_email
        self.html_message = html_message
        self.message = message
        self.recipients = recipients
        self.request = request
        self.context = context
        self.silently = silent

    def get_message_kwarg(self):
        if self.message:
            return {"message": self.message}
        return {"htm_message": self.render_template()}

    def render_template(self):
        rendered_template = render_to_string(
            self.template, {"request": self.request})
        return rendered_template

    def send(self):
        send_kwargs = {}
        send_kwargs.update(self.get_message_kwarg())
        send_kwargs.update({"context": self.context})
        send_kwargs.update({"recipient_list": self.recipients})
        send_kwargs.update({"from_email": self.from_email})
        send_kwargs.update({"subject": self.subject})
        try:
            send_mail(**send_kwargs)
        except SMTPException as e:
            if not self.silently:
                raise
            logger.error(f"MailSend Fail: {e}")
        logger.info(f"MailSend to {self.recipients}")
