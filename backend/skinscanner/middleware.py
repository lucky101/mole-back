import logging

from core.utils import get_or_none
from .models import Device


logger = logging.getLogger(__name__)


class DeviceLoad():

    def __init__(self, get_response):
        self.response = get_response

    def __call__(self, request):
        return self.response(request)

    def process_view(self, request, view_func, view_args, view_kwargs):
        device_token = view_kwargs.get("token")
        logger.debug(f"Process request with token {device_token}")
        if device_token:
            request.device = get_or_none(Device.objects, token=device_token)