from django.apps import AppConfig


class SkinscannerConfig(AppConfig):
    name = 'skinscanner'
