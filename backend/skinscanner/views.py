import logging

from django.conf import settings
from django.shortcuts import render
from django.core.mail import send_mail
from django.template.loader import render_to_string
from rest_framework import generics
from rest_framework.response import Response

from core import utils
from skinscanner.models import Result
from . import permissions, scanner
from . import serializers
from . import models

logger = logging.getLogger(__name__)


class RegisterDeviceAPIView(generics.CreateAPIView):
    permission_classes = (permissions.RegisterPermission,)
    serializer_class = serializers.DeviceSerializer
    queryset = models.Device.objects.all()


class ScanPhotoAPIView(generics.CreateAPIView):
    permission_classes = (permissions.RegistredDeviceRequired,)
    serializer_class = serializers.ScanSerializer
    queryset = models.Scan.objects.all()

    def perform_create(self, serializer):
        scan = serializer.save()
        scanner.Scanner.process_scan(scan)
        logger.info(f"Scan {scan}")


class ScanListAPIView(generics.ListAPIView):
    permission_classes = (permissions.RegistredDeviceRequired,)
    serializer_class = serializers.ScanSerializer

    def get_queryset(self):
        return models.Scan.objects.filter(device=self.request.device)


class ResultAPIView(generics.RetrieveAPIView):
    permission_classes = (permissions.RegistredDeviceRequired,)
    serializer_class = serializers.ResultSerializer
    queryset = models.Result.objects.all()

    def retrieve(self, request, pk, *args, **kwargs):
        instance = utils.get_or_none(models.Result.objects, scan__id=pk)
        if instance:
            serializer = self.get_serializer(instance)
            return Response(serializer.data)
        return Response({"error": "Results not found"})


class MailSendAPIView(generics.GenericAPIView):
    serializer_class = serializers.MailInputSerializer

    def post(self, request, token):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            # Send Mail
            try:
                scan_id = serializer.validated_data["id"]
                email = serializer.validated_data["mail"]
                result_obj = Result.objects.get(scan__id=scan_id)
                message = render_to_string("mail/message.html", {"result": result_obj})
                send_mail(subject="Результаты", from_email=settings.EMAIL_HOST_USER, recipient_list=(email,),
                          message="", html_message=message)
                return Response({"status": "ok"})
            except Exception as e:
                print(e)
                return Response({"status": "ko"})
        return Response({"status": "ko", "errors": serializer.errors})
