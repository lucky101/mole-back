from django.conf import settings
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from core.utils import unique_token_generate
from .models import Scan, Result, Device


class ScanSerializer(serializers.ModelSerializer):

    def create(self, vaildated_data):
        request = self.context.get("request")
        scan = Scan(**vaildated_data)
        scan.device = request.device
        scan.save()
        return scan

    def update(self, instance, vaildated_data):
        for field in vaildated_data:
            setattr(instance, field, vaildated_data[field])
        instance.save()
        return instance

    class Meta:
        model = Scan
        exclude = ("device",)


class ResultSerializer(serializers.ModelSerializer):

    def create(self, vaildated_data):
        return Result.objects.create(**vaildated_data)

    def update(self, instance, vaildated_data):
        for field in vaildated_data:
            setattr(instance, field, vaildated_data[field])
        instance.save()
        return instance

    class Meta:
        model = Result
        fields = "__all__"


class DeviceSerializer(serializers.ModelSerializer):
    token = serializers.ReadOnlyField()

    def create(self, vaildated_data):
        device = Device(**vaildated_data)
        token = unique_token_generate(Device.objects, "token")
        device.token = token
        device.save()
        return device

    def update(self, instance, vaildated_data):
        for field in vaildated_data:
            setattr(instance, field, vaildated_data[field])
        instance.save()
        return instance

    class Meta:
        model = Device
        fields = "__all__"


class MailInputSerializer(serializers.Serializer):
    mail = serializers.CharField(max_length=255)
    id = serializers.IntegerField()


class GrandTokenSerializer(serializers.Serializer):
    grand_token = serializers.CharField(max_length=255)

    def validate(self, vaildated_data):
        grand_token = vaildated_data.get("grand_token")
        if grand_token not in settings.GRAND_TOKENS:
            raise ValidationError({"grand_token": "this token is invalid"})
        return vaildated_data
