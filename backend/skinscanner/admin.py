from django.contrib import admin

from . import models

@admin.register(models.Scan)
class ScanAdminModel(admin.ModelAdmin):
    pass


@admin.register(models.Result)
class ResultAdminModel(admin.ModelAdmin):
    pass


@admin.register(models.Device)
class DeviceAdminModel(admin.ModelAdmin):
    pass