from django.db import models


class Scan(models.Model):
    ZONE_CHOICE = (
        ("none", "none"),
        ("face", "face"),
        ("head", "head"),
        ("chest", "chest"),
        ("neck", "neck"),
        ("body", "body"),
        ("butt", "butt"),
        ("back", "back"),
        ("armLeft", "armLeft"),
        ("armRight", "armRight"),
        ("foreArmLeft", "foreArmLeft"),
        ("foreArmRight", "foreArmRight"),
        ("handLeft", "handLeft"),
        ("handRight", "handRight"),
        ("hipLeft", "hipLeft"),
        ("hipRight", "hipRight"),
        ("shinLeft", "shinLeft"),
        ("shinRight", "shinRight"),
        ("footLeft", "footLeft"),
        ("footRight", "footRight"),
    )
    GENDER_CHOICE = (
        ("male", "Мужской"),
        ("female", "Женский")
    )
    SIDE_CHOICE = (
        ("front", "Передняя"),
        ("back", "Задняя")
    )

    def __str__(self):
        return f"{self.zone}-{self.side}-{self.gender}-{self.device}"

    zone = models.CharField(max_length=22, choices=ZONE_CHOICE)
    side = models.CharField(max_length=22, choices=SIDE_CHOICE)
    gender = models.CharField(max_length=22, choices=GENDER_CHOICE)
    mole_img = models.ImageField(upload_to="scans/photo/")
    black_dot = models.ImageField(upload_to="scans/photo/")
    red_dot = models.ImageField(upload_to="scans/photo/")
    green_dot = models.ImageField(upload_to="scans/photo/")
    blue_dot = models.ImageField(upload_to="scans/photo/")
    center_dot = models.ImageField(upload_to="scans/photo/")
    device = models.ForeignKey("Device", on_delete=models.CASCADE)



class Device(models.Model):
    uuid = models.CharField(max_length=255, unique=True)
    token = models.CharField(max_length=255, unique=True, blank=True)

    def __str__(self):
        return f"UUID:{self.uuid} TOKEN:{self.token}"


class Result(models.Model):
    STATUSES = (
        ("pending", "Подсчет"),
        ("ready", "Готово"),
        ("error", "Ошибка"),
    )

    scan = models.ForeignKey(Scan, on_delete=models.CASCADE)
    chance = models.DecimalField(max_digits=22, decimal_places=2)
    color = models.CharField(max_length=255)
    edge_count = models.PositiveIntegerField(default=0)
    pixels = models.PositiveIntegerField(default=0)
    status = models.CharField(choices=STATUSES, default=STATUSES[0][0], max_length=255)
    error_message = models.TextField(blank=True, null=True, default="Ошибки нет")
    countur = models.IntegerField(default=0)
    space = models.DecimalField(max_digits=36, decimal_places=6, default=0)
    max_diameter = models.FloatField(default=0.0)
    min_diameter = models.FloatField(default=0.0)
    form = models.CharField(max_length=255)

    def __str__(self):
        return f"{self.scan} {self.chance}"
