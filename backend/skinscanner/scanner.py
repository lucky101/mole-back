import logging
import os
import queue
import threading

from analyzer.exc import MoleAnalyserError
from analyzer.main import analyse_image
from .models import Result, Scan

logger = logging.getLogger(__name__)


class Scanner():
    RESULT_MODEL = Result
    SCAN_MODEL = Scan

    @classmethod
    def start_thread(cls):
        thrd = ScannerThread()
        thrd.setDaemon(True)
        thrd.start()

    @classmethod
    def process_scan(cls, elem):
        logger.info(f"{elem} adding to queue")
        result_obj = cls.create_result_obj(elem)
        ScannerThread.queue.put((elem.id, result_obj.id))

    def __init__(self, elem_id, result_id):
        self.result_obj = None
        self.result = None
        self.elem_id = elem_id
        self.result_id = result_id

    def get_elem(self):
        return Scan.objects.get(id=self.elem_id)

    def get_result(self):
        return Result.objects.get(id=self.result_id)

    def porcess(self):
        logger.info(
            f"Started process for id:{self.elem_id}")
        scan_obj = self.get_elem()
        mole_img_path = os.path.abspath(scan_obj.mole_img.path)
        black_dot_path = os.path.abspath(scan_obj.black_dot.path)
        red_dot_path = os.path.abspath(scan_obj.red_dot.path)
        green_dot_path = os.path.abspath(scan_obj.green_dot.path)
        blue_dot_path = os.path.abspath(scan_obj.blue_dot.path)
        center_dot_path = os.path.abspath(scan_obj.center_dot.path)
        self.result = analyse_image(mole_filename=mole_img_path, calib_black_filename=black_dot_path,
                                    calib_red_filename=red_dot_path, calib_green_filename=green_dot_path,
                                    calib_blue_filename=blue_dot_path, calib_gray_filename=center_dot_path)
        logger.info(f"Finished process for id:{self.elem_id}")

    @classmethod
    def create_result_obj(cls, scan_obj):
        logger.info(f"Create result for id:{scan_obj.id}")
        object_create_dict = {
            "scan": scan_obj,
            "chance": 0,
            "color": "none",
            "edge_count": 0,
            "pixels": 0,
            "status": Result.STATUSES[0][0]
        }
        result_obj = Result.objects.create(**object_create_dict)
        logger.info(f"Result is {result_obj}")
        return result_obj

    def save(self):
        logger.info(f"Save result for id:{self.elem_id}")
        result_obj = self.get_result()
        logger.info(f'INFO:{self.result["analysis_data"]["edge"]}')
        object_dict = {
            "chance": self.result["score"],
            "color": self.result["analysis_data"]["color"]["color"],
            "edge_count": len(self.result["analysis_data"]["edge"]["contour"]),
            "pixels": self.result["analysis_data"]["edge"]["area_pixels"],
            "contur": self.result["analysis_data"]["edge"]["contour"],
            "space": self.result["analysis_data"]["edge"]["area_real"],
            "status": Result.STATUSES[1][0],
            "form": self.result["analysis_data"]["edge"]["form"],
            'min_diameter': self.result["analysis_data"]["min_diameter"],
            'max_diameter': self.result["analysis_data"]["max_diameter"],
        }
        for elem in object_dict:
            setattr(result_obj, elem, object_dict[elem])
        result_obj.save()
        logger.info(f"Result is {self.result}")

    def error(self, e):
        logger.info(f"Error result for id:{self.elem_id}")
        result_obj = self.get_result()
        result_obj.error_message = str(e)
        result_obj.status = Result.STATUSES[2][0]
        result_obj.save()
        logger.info(f"Error saved {self.result}")


class ScannerThread(threading.Thread):
    queue = queue.Queue()
    threads = []

    def __init__(self):
        super().__init__()
        self.threads.append(self)

    def run(self):
        logger.info("Thread started")
        while True:
            elem_and_result = self.queue.get()
            logger.info("Thread get element")
            scanner = Scanner(elem_and_result[0], elem_and_result[1])
            try:
                scanner.porcess()
                scanner.save()
            except Exception as e:
                scanner.error(e)
                print(e, "THREAD FALLEN")
            else:
                scanner.save()
            logger.info("Thread element proceed and saved")
            self.queue.task_done()


Scanner.start_thread()
# Scanner.start_thread()
# Scanner.start_thread()
# Scanner.start_thread()
# Scanner.start_thread()
logger.info("Total threads %d" % len(ScannerThread.threads))
