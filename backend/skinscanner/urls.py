from django.urls import path

from . import views


urlpatterns = [
    path("register/", views.RegisterDeviceAPIView.as_view(), name="register"),
    path("result/<pk>/", views.ResultAPIView.as_view(), name="result"),
    path("scan/", views.ScanPhotoAPIView.as_view(), name="scan"),
    path("scans/", views.ScanListAPIView.as_view(), name="scans"),
    path("mail/", views.MailSendAPIView.as_view(), name="maill"),
]
