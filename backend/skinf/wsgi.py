import os
import sys

home_path = os.path.expanduser("~")
virtenv_path = os.path.join(home_path, 'virtenv')
virtualenv = os.path.join(virtenv_path, 'bin/activate_this.py')
project_path = os.path.join(home_path, "ROOT", "skinfinder", "webapp")
sys.path.insert(0, project_path)
try:
    if sys.version.split(' ')[0].split('.')[0] == '3':
        exec(compile(open(virtualenv, "rb").read(), virtualenv, 'exec'), dict(__file__=virtualenv))
    else:
        execfile(virtualenv, dict(__file__=virtualenv))
except IOError:
    pass

from django.core.wsgi import get_wsgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'skinf.settings')

application = get_wsgi_application()
