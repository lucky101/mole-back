import sys
import operator
import math
import json
from operator import itemgetter

import numpy as np

"""
Strong Classifier trained with Gentle AdoBoost classifier decision stump
"""

class Classifier(object):
    def __init__(self, learners_count, labels_count):
        self.learners_count = learners_count
        self.model = None
        self.labels_count = labels_count

    def clean_model(self): 
        self.model = {
            'featuresIdx': [], 
            'th': [] , 
            'a': [], 
            'b': []
        }
        self.labels_count = 0

    def get_uniq_labels(self, y):
        return list(set(y))

    def sort_x_y(self, x, y): 
        rows, cols = len(x), len(x[0])
        res_x, res_y = [], []

        for j in range(0, cols): 
            index = list(range(rows))
            xtemp = x[j * rows:(j+1) * rows]

            qsindex(xtemp, index, 0, rows - 1)
            
            for i in range(0, rows):
                res_x.append(xtemp[i])
                res_y.append(int(y[index[i]]))
                idX.append(index[i])

        return res_x, res_y, idX

    def save(self, filename):
        """ Save the trained classifier to the JSON file """

        with open(filename, 'w') as f:
            json.dump({
                'featuresIdx': self.model['featuresIdx'], 
                'th': self.model['th'],
                'a': self.model['a'],
                'b': self.model['b']
            }, f)

    def load(self, filename):
        """ Load the classifer values from the JSON file """ 

        with open(filename, 'r') as f:
            loaded = json.load(f)
            
        self.model = {
            'featuresIdx':  loaded["featuresIdx"],
            'th':  loaded["th"],
            'a': loaded["a"],
            'b': loaded["b"]
        }
        
    def train(self, x_input, y_input):
        n_rows, n_cols  = len(x_input), len(x_input[0])

        self.clean_model()

        labels = self.get_uniq_labels(y_input)
        self.labels_count = len(labels)

        assert self.labels_count >= 1, 'Labels cound can\'t be less than 2'
        assert self.labels_count != 2, 'Binary classification case was dropped in this py implementation - see the full C code'

        # Transponation and sort optimizations was dropped in this py implementation

        x = np.array(x_input, dtype=np.float)
        y = np.array(y_input, dtype=np.float)

        for label_idx, label in enumerate(labels):
            # Weights inital filling
            weights = [1.0 / n_rows] * n_rows

            for _ in range(0, self.learners_count):
                min_error, th_optimal, a_optimal, b_optimal, featuresIdx_optimal = sys.maxsize, .0, .0, .0, 0
            
                for col_idx in range(0, n_cols):
                    x_col = x[:,col_idx]
                    y_matches = list(map(lambda item: 1.0 if item == label else -1.0, y))

                    w_total_sum = sum(weights)
                    yw_total_sum = sum(map(operator.mul, y_matches, weights))
                        
                    w_sum, yw_sum = .0, .0

                    for row_idx, (w, y_match) in enumerate(zip(weights, y_matches)):
                        w_sum += w
                        yw_sum += y_match*w

                        btemp = yw_sum/w_sum
                        atemp = (yw_total_sum - yw_sum)/(1.0 - w_sum) - btemp if w_sum != 1.0 else (yw_total_sum - yw_sum) - btemp
                        
                        error = w_total_sum - 2.0*atemp*(yw_total_sum - yw_sum) - 2.0*btemp*yw_total_sum + (atemp**2 + 2.0*atemp*btemp)*(1.0 - w_sum) + btemp**2
                        
                        if error < min_error:
                            # New optimal case					
                            min_error = error

                            featuresIdx_optimal = col_idx
                            th_optimal = (x_col[row_idx] + x_col[row_idx + 1])/2 if row_idx < n_rows - 1 else x_col[row_idx]
                            a_optimal, b_optimal = atemp, btemp			
                
                # Best parameters for weak-learner 
                self.model['featuresIdx'].append(featuresIdx_optimal + 1)
                self.model['th'].append(th_optimal)
                self.model['a'].append(a_optimal)
                self.model['b'].append(b_optimal)
                
                # Weigth's update 
                for i in range(0, n_rows):
                    fm = a_optimal*(x[i, featuresIdx_optimal] > th_optimal) + b_optimal
                    weights[i] *= math.exp(-fm if y[i] == label else fm)
                
                w_sum = sum(weights)
                coeff = 1.0/w_sum
                
                for i in range(0, n_rows):
                    weights[i] *= coeff


    def predict(self, x):
        x = np.array(x, dtype=np.float)
        rows, cols = x.shape[0], 1
        featureIdx, th, a, b = self.model['featuresIdx'], self.model['th'], self.model['a'], self.model['b']

        assert self.labels_count != 2, 'Binary classification case support was dropped in this py implementation - see the full C code'

        nm = 0
        yest = []
        
        for row_idx in range(0, rows):
            best_sum, best_sum_idx = -sys.maxsize, 0 
            
            for label_idx in range(0, self.labels_count):
                sum = .0
                offset = self.learners_count * label_idx
                
                for t in range(offset, offset + self.learners_count):
                    sum += a[t] * (x[featureIdx[t] - 1 + cols * row_idx] > th[t]) + b[t]
                
                if sum > best_sum:
                    best_sum, best_sum_idx = sum, label_idx

            yest.append(best_sum_idx)
            nm += self.labels_count

        
        best = min(enumerate(yest), key=itemgetter(1))[0] 

        return best