import cv2 as cv
import numpy as np

from ..utils.color import img_to_gray
from ..utils.debug import _debug_imshow, _debug_show_contours, _debug_print, _debug_imshow_multiple, _debug_draw_contours
from ..utils.morphology import imfill_holes, imclearborder 
from ..utils.contours import get_eccentricy, get_min_maj_axis

FORM_CIRCLE = 0
FORM_OVAL = 1
FORM_OTHER = 2

def get_mole_contour(mole_binary):
    """ Gets contour from mole binary image """

    _, contours, _ = cv.findContours(mole_binary, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_NONE)
    max_contour = max(contours, key = cv.contourArea)
    
    return max_contour

def get_mole_binary(gray):
    """ Gets mole binary image by grayscale input """
    
    # Apply threshold
    #_, thresholded = cv.threshold(gray, .7*255, 255, cv.THRESH_BINARY)
    thresholded = gray

    # Apply regiongrow
    seed, threshold = 6, 6
    regiongrowed = cv.inRange(thresholded, seed - threshold, seed + threshold)

    # Apply morphology
    dilated = cv.dilate(regiongrowed, cv.getStructuringElement(cv.MORPH_CROSS, (3, 3)))
    filled = imfill_holes(dilated) 
    cleared = imclearborder(filled, 8)
    mole_binary = cv.erode(cleared, cv.getStructuringElement(cv.MORPH_ELLIPSE, (7, 7)), iterations=2) # NOTE: original form - diamond

    mole_binary = cv.dilate(mole_binary, cv.getStructuringElement(cv.MORPH_ELLIPSE, (38, 38))) # NOTE: original form - disk
    mole_binary = cv.erode(mole_binary, cv.getStructuringElement(cv.MORPH_ELLIPSE, (38, 38))) # NOTE: original form - disk

    # Debug imshow
    _debug_imshow_multiple('Get mole binary', {
      'Gray (input)': gray,
      'Thresholded': thresholded,
      'Regiongrowed': regiongrowed,
      'Binary (result)': mole_binary
    }, rows=2, cols=2)

    return mole_binary  

def get_mole_area(mole_binary):
    """ Returns mole area in pixels """
    return np.sum(mole_binary == 255)

def get_mole_diameters(contour):
    """ Returns mole min and max diameters in pixels """
    maj_axis_len, min_axis_len = get_min_maj_axis(contour)
    min_diameter, max_diameter = round(min_axis_len, 2), round(maj_axis_len, 2)

    return min_diameter, max_diameter 

def get_mole_shape(contour, binary):
    """
    Returns mole shape: CIRCLE, OVAL or OTHER

    Parameters:
      coutour - mole contour
      binary - mole binary image

    Algorithm:
    - if difference between real mole area and convex hull area  
      is more than border value - the shape is OTHER else the shape is ellipse
    - if eccentricy of the ellipse is less than border value it's CIRCLE 
    - ele the shape is OVAL

    What is convex hull: https://www.youtube.com/watch?v=ZFxFKABnXN0
    What is exxentricy: https://www.youtube.com/watch?v=pT104LRN8Ns
    """

    eccentricy = get_eccentricy(contour)

    convex_area = cv.contourArea(cv.convexHull(contour))
    area = get_mole_area(binary)

    if abs(area - convex_area) >= 5000:
        return FORM_OTHER
    elif eccentricy <= .6:
        return FORM_CIRCLE
    else: 
        return FORM_OVAL

def get_mole_sharpness():
    """ Returns mole image sharpness """
    return 1 # TODO

def analyse(img):
    # Convert image to grayscale
    gray = img_to_gray(img)

    # Convert image to binary
    binary = get_mole_binary(gray)

    # Detect mole contour
    contour = get_mole_contour(binary)

    # Debug imsow
    _debug_imshow_multiple('Edge analysis', {
      'Input': img,
      'Gray': gray,
      'Binary': binary,
      'Mole Contour': _debug_draw_contours(binary, [contour])
    }, rows=2, cols=2)

    # Get mole form
    form = get_mole_shape(contour, binary)
    _debug_print(f'Mole form: {form}')
    
    # Get mole diameter (in pxels)
    min_diameter, max_diameter = get_mole_diameters(contour)
    _debug_print(f'Min mole diameter (px): {min_diameter}')
    _debug_print(f'Max mole diameter (px): {max_diameter}')

    # Getting mole area (in pixels)
    area = get_mole_area(binary) 
    _debug_print(f'Mole area (px): {area}')

    # Getting sharpness
    sharpness = get_mole_sharpness() 
    _debug_print(f'Mole sharpness: {sharpness}')

    return {
        'contour': contour,
        'form': form,
        'min_diameter_pixels': min_diameter,
        'max_diameter_pixels': max_diameter,
        'area_pixels': area,
        'sharpness': sharpness
    }
