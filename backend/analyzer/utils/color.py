import cv2 as cv


def img_mean_color(img):
    b, g, r = cv.split(img)
    av_b = b.mean(axis=0).mean(axis=0)
    av_g = g.mean(axis=0).mean(axis=0)
    av_r = r.mean(axis=0).mean(axis=0)

    return av_r, av_g, av_b

def img_to_gray(img):
    gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)  
    enhanced = cv.equalizeHist(gray) 

    return enhanced