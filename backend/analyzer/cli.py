import os 
import sys
import pprint

import main as full_analysis


if __name__ == '__main__':
    if len(sys.argv) != 2:
        raise Exception('Wrong args. Usage: python cli.py MY_IMG_PATH')

    img_path = sys.argv[1]

    if not os.path.exists(img_path):
        raise Exception(f'File {img_path} doesn\'t exists')

    result = full_analysis.analyse_image(img_path)

    pprint.pprint(result)