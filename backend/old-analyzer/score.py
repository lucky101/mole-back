from math import pi as PI

from utils.debug import _debug_print


def get_max_diameter_score(d):
    return 1 if d <= 2 else \
           2 if 2 < d <= 4 else \
           3 if 4 < d <= 6 else \
           4 if 6 < d <= 8 else \
           5 if 8 < d <= 10 else \
           6

def get_area_score(area):
    return 1 if area <= PI else \
           2 if PI < area <= 4*PI else \
           3 if 4*PI < area <= 8*PI else \
           4 if 8*PI < area <= 16*PI else \
           5 if 16*PI < area <= 25*PI else \
           6

def calc_result_score(form, sharpness, max_diameter_mm, area_mm2, color_score):
    W_FORM = 1.3
    W_SHARPNESS = 0.1
    W_COLOR = 0.5
    W_DIAMETER = 0.5
    W_AREA = 0.5

    M_ADD_COEFF = 1/11.7

    diameter_score = get_max_diameter_score(max_diameter_mm)
    area_score = get_area_score(area_mm2)

    _debug_print('---- M formula data ----')
    _debug_print('W_FORM:', W_FORM)
    _debug_print('form:', form)
    _debug_print('W_SHARPNESS:', W_SHARPNESS)
    _debug_print('sharpness:', sharpness)
    _debug_print('W_COLOR:', W_COLOR)
    _debug_print('color_score:', color_score)
    _debug_print('W_DIAMETER:', W_DIAMETER)
    _debug_print('diameter_score:', diameter_score)
    _debug_print('W_AREA:', W_AREA)
    _debug_print('area_score:', area_score)

    M = W_FORM*form + W_SHARPNESS*sharpness + W_COLOR*color_score + W_DIAMETER*diameter_score + W_AREA*area_score
    
    _debug_print('---- Score formula data ----')
    _debug_print('M_ADD_COEFF:', M_ADD_COEFF)
    _debug_print('M:', M)

    P = M_ADD_COEFF * M *100

    _debug_print('--- Score (P):', P)

    return P      