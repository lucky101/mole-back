import unittest
import os

import cv2 as cv

import calibration
import main as full_analysis
import analysis.edge as edge_analysis
import analysis.color as color_analysis
from utils.debug import _debug_imshow
from exc import MoleAnalyserError


PROJ_DIR = os.path.dirname(os.path.realpath(__file__))

@unittest.skip
class TestCalibration(unittest.TestCase):
    def test_calibration(self):
        # Set image filepath (and check file existance)
        IMG_FILEPATH = os.path.join(PROJ_DIR, '../old/img/2519.bmp')
        
        self.assertTrue(os.path.exists(IMG_FILEPATH))

        # Load image
        img = cv.imread(IMG_FILEPATH)
        self.assertIsNotNone(img)
        _debug_imshow('Test: input', img)

        calibration.calibrate(img)


@unittest.skip
class TestEdgeAnalyser(unittest.TestCase):
    def test_edge_analyser(self):
        # Set image filepath (and check file existance)
        IMG_FILEPATH = os.path.join(PROJ_DIR, '../old/img/2519.bmp')
        self.assertTrue(os.path.exists(IMG_FILEPATH))

        # Load image
        img = cv.imread(IMG_FILEPATH)
        self.assertIsNotNone(img)
        _debug_imshow('Test: input', img)

        # Run edge analysis
        edge_analysis.analyse(img)


@unittest.skip
class TestColorAnalysis(unittest.TestCase):
    def test_classifier(self):
        mole_color = (0.4323, 0.2417, 0.1687)
        rgb_colors = {
            'r': (0.6420, 0.2655, 0.2899), 
            'g': (0.2738, 0.5191, 0.3389), 
            'b': (0.2806, 0.3926, 0.5962) 
        }

        res = color_analysis.analyse(rgb_colors, mole_color)

@unittest.skip
class TestFullAnalysis(unittest.TestCase):
    '''
    Test for the full analyser on a normal input image (mole present, references presets)
    Checks:
        - The analyser runs with no exceptions
        - True format of the analyser output data
        - Report images existance
    '''

    def test_edge_analyser(self):
        # Set image filepath (and check file existance)
        IMG_FILEPATH = os.path.join(PROJ_DIR, '../old/img/2519.bmp')
        self.assertTrue(os.path.exists(IMG_FILEPATH))

        # Run analysis
        res = full_analysis.analyse_image(IMG_FILEPATH)

        # Analysis result format & types check
        self.assertIn('score', res)
        self.assertIn('report_images', res)
        self.assertIn('analysis_data', res)

        self.assertIsInstance(res['score'], float)
        self.assertIsInstance(res['report_images'], dict)
        self.assertIsInstance(res['analysis_data'], dict)

        for name, path in res['report_images'].items():
            self.assertIsInstance(name, str)
            self.assertIsInstance(path, str)

        # Check report images existance
        for _, path in res['report_images'].items():
            self.assertTrue(os.path.exists(path))

@unittest.skip
class TestNoReferencesCase(unittest.TestCase):
    '''
    Test for the full analyser on bad input image
    Check:
        - Analysis exception raising
    '''

    def test_main(self):
        # Set image filepath (and check file existance)
        IMG_FILEPATH = os.path.join(PROJ_DIR, 'test_images/no_references.jpg')
        self.assertTrue(os.path.exists(IMG_FILEPATH))

        # Run analysis
        with self.assertRaises(MoleAnalyserError):
            full_analysis.analyse_image(IMG_FILEPATH)

class TestManyImages(unittest.TestCase):
    '''
    '''

    def test_main(self):
        # Set image filepath (and check file existance)
        inputs = [
            {
                'mole_filename': os.path.join('test_images', '1_mole.png'),
                'calib_red_filename': os.path.join('test_images', '1_calib_red.png'),
                'calib_green_filename': os.path.join('test_images', '1_calib_green.png'),
                'calib_blue_filename': os.path.join('test_images', '1_calib_blue.png'),
                'calib_black_filename': os.path.join('test_images', '1_calib_black.png'),
                'calib_gray_filename': os.path.join('test_images', '1_calib_gray.png'),
            }
        ]
        
        for analyse_input in inputs:
            for kwname, path in analyse_input.items():
                fullpath = os.path.join(PROJ_DIR, path)
                self.assertTrue(os.path.exists(fullpath))
                analyse_input[kwname] = fullpath

            full_analysis.analyse_image(**analyse_input)

if __name__ == '__main__':
    unittest.main()