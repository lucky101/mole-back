import cv2 as cv

import calibration 
from score import calc_result_score
from utils.files import save_report_image
from utils.morphology import imfill_holes, imclearborder
from utils.contours import get_min_maj_axis, get_eccentricy, draw_contours
from utils.debug import _debug_print
import analysis.color as color_analysis
import analysis.edge as edge_analysis  
import preprocessing


def analyse_image(mole_filename, calib_red_filename, calib_green_filename, calib_blue_filename, calib_black_filename, calib_gray_filename):
    # Load images 
    print(mole_filename)
    mole_img = cv.imread(mole_filename)
    calib_red_img = cv.imread(calib_red_filename)
    calib_green_img = cv.imread(calib_green_filename)
    calib_blue_img = cv.imread(calib_blue_filename)

    assert mole_img is not None
    assert calib_red_img is not None
    assert calib_green_img is not None
    assert calib_blue_img is not None

    # Apply preprocessing
    preprocessed_mole_img = preprocessing.apply(mole_img)

    # Callibratie
    rgb_colors, pixel_to_mm_rate = calibration.calibrate(calib_red_img, calib_blue_img, calib_green_img)
    pixel_to_mm2_rate = pixel_to_mm_rate ** 2 

    # Edge analysis
    edge_data = edge_analysis.analyse(preprocessed_mole_img)

    # Convert mole diameters to mm
    min_diameter, max_diameter = edge_data['min_diameter_pixels'], edge_data['max_diameter_pixels']
    min_diameter_real, max_diameter_real = min_diameter * pixel_to_mm_rate, max_diameter * pixel_to_mm_rate
    _debug_print(f'Pixel -> mm rate: {pixel_to_mm_rate}')
    _debug_print(f'Min mole diameter (mm): {min_diameter_real}')
    _debug_print(f'Max mole diameter (mm): {max_diameter_real}')

    # Convert the area to mm2
    area = edge_data['area_pixels']
    area_real = area * pixel_to_mm2_rate
    _debug_print(f'Pixels -> mm2 ratio: {pixel_to_mm2_rate}')
    _debug_print(f'Mole area (mm2): {area_real}')

    # Getting mole image
    x, y, w, h = cv.boundingRect(edge_data['contour'])
    mole_img = mole_img[x:x+w, y:y+h]

    # Color analysis
    color_data = color_analysis.analyse(rgb_colors, mole_img)

    # Count summary score
    result_prob = calc_result_score(
        form=edge_data['form'], 
        sharpness=edge_data['sharpness'],
        max_diameter_mm=edge_data['max_diameter_pixels'] * pixel_to_mm_rate, 
        area_mm2=edge_data['area_pixels'] * pixel_to_mm2_rate, 
        color_score=color_data['malignization_prob']
    )

    # Save report image
    report_image_filepath = save_report_image(draw_contours(mole_img, [edge_data['contour']]))

    return {
        'score': result_prob,
        'report_images': {
            'contour': report_image_filepath
        },
        'analysis_data': {
            'edge': edge_data,
            'color': color_data
        }
    }