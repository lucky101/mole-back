import os
import json

from utils.color import img_mean_color
from .classifier import Classifier


FILE_DIR = os.path.dirname(os.path.realpath(__file__))
CLASSIFIER_FILES = {
    'classifier': os.path.join(FILE_DIR, 'trained.json'),
    'out_descr': os.path.join(FILE_DIR, 'classifier_results.json')
}

def train_classifier(filename):
    """ (Depracted - see load_classifier) Create classifier object and train it """

    # Read file lines
    with open(filename) as f:
        lines = f.readlines()

    # x - data, y - labels
    x, y = [], []

    # Split file data into labels (first column) and data (other columns)
    for line in lines:
        label, *data = line.split()
        y.append(label)
        x.append(data)

    # Create classifier object
    classifier = Classifier(learners_count=50)

    # Load data to classifier
    classifier.train(x, y)

    return classifier

def load_classifier(filename):
    """ Load classifier object """

    # Create classifier object
    classifier = Classifier(learners_count=50, labels_count=25)

    # Load classifier data from file
    classifier.load(filename)

    return classifier

def predict_to_result(predict):
    """ Convert predict (class index) to result data """

    with open(CLASSIFIER_FILES['out_descr'], encoding='utf-8') as f:
        results_match = json.load(f) 

    try:
        result = results_match[str(predict)] 
    except KeyError:
        raise Exception(f'Unexpected classifier result: {predict}. All possible outputs are listed in {CLASSIFIER_FILES["out_descr"]}')

    color, intensity, malignization_prob = result['color'], result['intensity'], result['malignization_prob']

    return color, intensity, malignization_prob

def get_color_consistency(img):
    return 1 # TODO

def analyse(rgb_colors, mole_img):
    """
    Analyse mole color (relative to rgb references) using classifier 
    
    Input: 
      rgb_colors - Array of 3 rgb tuples for true RGB colors on the image 
                   (e.g. [(), (), ()])
      mole_img - Mole image

    Return:
      Dict with fields: 
        color - name of the mole's color (in russian), e.g. "коричневый"
        intensity - name of mole's intensity (in russian), e.g. "светлая"
        malignization_prob - probability of malignization for the mole (int from 1 to 6)
    """

    # Load classifier
    classifier = load_classifier(CLASSIFIER_FILES['classifier'])

    # Getting mole average color
    mole_color = img_mean_color(mole_img)

    # Prepare input vector
    Nr, Ng, Nb = mole_color
    (Rr, Rg, Rb), (Gr, Gg, Gb), (Br, Bg, Bb) = rgb_colors['r'], rgb_colors['g'], rgb_colors['b']

    input_vec = [Nr/Rr, Ng/Rg, Nb/Rb, Nr/Gr, Ng/Gg, Nb/Gb, Nr/Br, Ng/Bg, Nb/Bb]

    # Classify
    predict = classifier.predict(input_vec) 

    # Convert predict to result
    color, intensity, malignization_prob = predict_to_result(predict)

    return {
        'color': color, 
        'intensity': intensity, 
        'malignization_prob': malignization_prob
    }