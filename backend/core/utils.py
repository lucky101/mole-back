import random
import string


def get_or_none(queryset, **filters):
    filtred_queryset = queryset.filter(**filters)
    if filtred_queryset.count() == 1:
        return filtred_queryset.first()
    else:
        return None


def unique_token_generate(queryset, token_field):
    object_with_token = True
    while object_with_token:
        token = "".join(random.choice(string.ascii_lowercase +
                                      string.digits) for _ in range(22))
        filter_kwarg = {
            token_field: token
        }
        object_with_token = get_or_none(queryset, **filter_kwarg)
    return token
